Title: Invitations, pour le 19 mai 2018
Date: Sun Apr 22 15:12:21 CEST 2018
Modified: Sun Apr 22 15:12:21 CEST 2018
Tags: programme
Category: invitations
Lang: fr

<img align="right" width="300" src="/images/img-19-05-2018.svg.png" alt="Sympa, les ours ! Je vais faire les invitations avec Scribus !"/>
Par rapport au programme initial, il y a quelques modifications. Durant
la rencontre du 21 avril, nous nous sommes rendus compte que la
liaison Internet disponible à la Médiathèque est de bonne qualité, et facilement
disponible, grâce à Éric qui a déployé un point d'accès très efficace.

Cependant, comme nous avons dû télécharger quelques fichiers (par exemple :
quelques personnes ont mis à jour Firefox pour pouvoir installer le
*plugin* LightBeamer), nous nous sommes rendus compte qu'il faut éviter de
demander chacun des débits de données importants quand on est nombreux.

Par conséquent, la démonstration de communication vidéo décentralisée
sera probablement réduite.


Le programme sera donc :

  * Démarrage d'un PAD pour structurer les ordres du jour des séances à
venir. Ça permet de compléter le présent programme : les « habitués » peuvent prévoir des activités spécifiques ;
  * Découverte de « Scribus » : on s'exerce à présenter une invitation
ou un menu de fête.
  * Les participants sont invités à préparer un petit texte pour la
fois suivante, en vue d'une mise en page.
  * Pour celles et ceux qui viennent avec leur ordinateur portable et
    utiliseront le système d'exploitation de leur disque dur, c'est une
	bonne idée de pré-installer le lociciel libre et gratuit Scribus, à
	télécharger de préférence depuis
	https://www.scribus.net/downloads/stable-branch/
  * Des ressources toutes prêtes sont éventuellement disponibles,
    <a href="/fichiers-de-donnees-pour-le-19-mai-2018.html">En cliquant ici</a>.

Les clés USB prêtées pour fonctionner dans un environnement de logiciels
libres seront basées sur une *variante* du système Debian déjà présenté à
la précédente rencontre. Cette variante permet une installation sur disque
dur, si on veut, mais elle permet aussi de fonctionner sans rien changer
au disque dur de l'ordinateur.

Si vous souhaitez diffuser des invitations, au format A6, voici une planche de
quatre invitations à imprimer puis à découper sur papier ordinaire :

<a href="/images/invitation-19-05-2018.pdf" title="Cliquer pour un document imprimable, au format PDF">
<img src="/images/invitation-19-05-2018.svg.png" alt="invitations" style="width: 600px;"/>
</a>
