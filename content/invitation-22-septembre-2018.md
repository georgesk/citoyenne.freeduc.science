Title: Invitations, pour le 22 septembre 2018
Date: 2018/09/17
Modified: 2018/09/17
Tags: programme
Category: invitations
Lang: fr


<div>
<img align="right" width="300" src="/images/crypto.png" alt="échanges de messages chiffrés"/>
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Inkscape" src="/images/inkscape.png">
</div>

# Échanger sans être espionné #

Les participants s'entraînent à l'usage de signature électronique.

C'est possible de se préparer de deux façons :

 * quand on dispose de la petite clé vive sous GnuLinux, tout le matériel
   de cryptographie est déjà là.
 * quand on utilise Windows, il est préférable d'installer d'avance
   [GPG4Win](https://www.gpg4win.org/about.html), l'explication se fera
   à l'aide de la page web du site 
   [securityinabox.org](https://securityinabox.org/fr/guide/thunderbird/windows/)
   donc il vaut mieux installer par avance le logiciel de messagerie
   [ThunderBird](https://www.mozilla.org/fr/thunderbird)
  
Attention, nous n'aurons pas le temps de traiter ce sujet à fond !
Ce sera juste une démonstration en quelques temps:

  * je crée des clés crypto
  * je publie ma clé crypto publique
  * je poste mon message, avec une signature numérique
  * une autre personne récupère la clé publique
  * elle reçoit mon message, et vérifie la signature
  
# Créer des images simples (Logos, etc.) #

Prise en main rapide du logiciel [Inkscape](https://inkscape.org/fr/).
Pensez à amener des concepts
intéressants : par exemple, le concept « nous voulons des coquelicots »,
ça se traduit assez facilement en utilisant des formes arrondies de couleurs
rouges.

Nous créerons un petit logo simple, en toute liberté, en utilisant 
quelques-unes des fonctions de base d'Inkscape.
