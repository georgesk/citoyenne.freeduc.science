Title: Invitations, pour le 20 octobre 2018
Date: 2018/10/07
Modified: 2018/10/07
Tags: programme
Category: invitations
Lang: fr


<div>
<img align="right" height="240" src="/images/cw.png" alt="Wikimedia commons"/>
<img style="float: right; margin-left: 1ex; height: 240px;" alt="Inkscape" src="/images/inkscape.png">
</div>

  
# Retour sur Inkscape, accès à des millions d'œuvres #

Les licences 
[Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR)
permettent de gérer un ensemble croissant de ressources « biens communs ».

On considère comme « libre » la licence **CC BY-SA**, souvent utilisée. Cette
licence permet de copier et distribuer librement une œuvre, sous deux
contraintes :

1. On doit citer correctement les auteur.e.s ;
2. Les œuvres dérivées doivent être publiées sous la même licence.

Une recherche avec les mots-clés « `svg cc by-sa` » montre
immédiatement où sont les plus gros *gisements* de dessins vectoriels
sous licence libre : les liens pointent vers **Wikipedia** ou plus
précisément, [Wikimedia Commons](https://commons.wikimedia.org), le
dépôt utilisé par Wikipedia, qui contenait au moment de la rédaction
de cet article, une collection de 49 993 893 medias utilisables
librement.

Visitez aussi [http://data.abuledu.org](http://data.abuledu.org),
le site maintenu par l'association
[AbulÉdu](http://abuledu-fr.org/comment-nous-aiderparticiper/) qui
renseigne l'accès à ces medias libres de façon très structurée, en
français, avec des outils prêts à utiliser par les élèves et professeurs
des écoles.

**Nous verrons comment reprendre une œuvre sous licence libre à l'aide 
d'Inkscape et la retravailler superficiellement.**


