Title: Le programme des ateliers 2018
Date: Fri Feb 16 17:10:31 CET 2018
Modified: Fri Feb 16 17:10:31 CET 2018
Tags: programme
Category: accueil
Lang: fr

## Samedi 24 mars 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex;" alt="Affiche du film" src="/images/nothing-to-hide.jpg">

  * Projection du film : **Nothing to Hide** *(2017, par Marc MEILLASSOUX, Mihaela
GLADOVIC)*, 
[voir le résumé sur allocine.fr](http://www.allocine.fr/film/fichefilm_gen_cfilm=253027.html) ;
  * Présentation d'outils pour reprendre le contrôle de nos
données personnelles : moteur de recherche plus respectueux, plugins anti-espionnage, etc.
  * Recettes pour que chacune et chacun puisse mieux protéger sa vie
privée ; les participants sont invités à reproduire chez eux ce qu'ils
ont pratiqué.

## Samedi 21 avril 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Etherpad" src="images/etherpad-500x295.png">

  * Retour sur la protection des données personnelles : « avez-vous
réussi à utiliser... », « recevez-vous toujours les mêmes publicités
invasives... », etc. ;
  * Prise en main d'outils collaboratifs : les
    [PADs](https://framapad.org/)
  * Les participants démarrent un ou plusieurs PADs sur un ou
plusieurs sujet, par exemple : recettes « maison », préparation d'un
évènement.

## Samedi 19 mai 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Scribus" src="images/96px-Scribus_logo.svg.png">

  * Démarrage d'un PAD pour structurer les ordres du jour des séances à
venir. Ça permet de compléter le présent programme : les « habitués » peuvent prévoir des activités spécifiques ;
  * Prise en main de logiciels de
[communication vidéo décentralisés](https://framatalk.org). Un serveur
assure la coordination de deux ordinateurs, après quoi l'échange
audio/vidéo continue en mode pair à pair, sans espionnage ;
  * Découverte de « Scribus » : on s'exerce à présenter une invitation
ou un menu de fête.
  * Les participants sont invités à préparer un petit texte pour la
fois suivante, en vue d'une mise en page.

## Samedi 23 juin 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="GNUpg" src="images/b_1_q_0_p_0.jpg.png">

  * Retour sur Scribus : on prend un texte, qu'on fond dans une
    maquette un peu sophistiquée. Deux exemples :
	* (a) dans une maquette de bulletin d'association,
	* (b) dans une maquette de mini-livre (après passage dans
	l'imprimante, une incision et un pliage de la seule feuille
	imprimée, ça fait un document de huit pages).
  *	Mini-conférence sur la cryptographie : comment chiffrer de façon
	sûre un message, comment signer de façon infalsifiable un message
	en clair.
	
## Samedi 22 septembre 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Inkscape" src="images/inkscape.png">

  * Les participants créent leur clés cryptographiques et s'entraînent
    à chiffrer, signer numériquement.
  * Un petit débat : quand doit-on chiffrer ? Par exemple : dans
    une association, la trésorière envoie un courriel à la présidente,
	doit-elle le chiffre ?
  * Prise en main élémentaire du logiciel
	[Inkscape](http://inkscape.fr/) : création d'un logo associatif
	simple, puis d'une image plus riche ... quelles sont les bonnes
	question à se poser avant de donner son travail à un imprimeur ?

## Samedi 20 octobre 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Wikimedia commons" src="images/cw.png">

  * Retour sur [Inkscape](http://inkscape.fr/) : comment récupérer des
    milliers de dessins et de symboles, disponibles sous licences
    libres Creative Commons.
  * Les participants s'entraînent à combiner de plusieurs façons des
	dessins et symboles récupérés ainsi. Technique de vectorisation
	d'images, pour fabriquer des schémas qu'on peut agrandir et
	styliser à souhait.
  * Visite de [commons.wikimedia.org](https://commons.wikimedia.org) ;
	les participants sont invités à préparer des photos de monuments
	ou de paysages qui manquent dans cette base de données mondiale.
	
## Samedi 17 novembre 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="Wikipedia" src="images/wikipedia.png">

  * [Wikipedia, l'Encyclopédie collaborative](https://fr.wikipedia.org/). Visite
    de quelques pages, de l'historique de ces pages : il s'agit de
    comprendre
	* comment les pages de Wikipedia se fabriquent,
	* qui les fabrique, comment on les protège contre le vandalisme,
	* de quelle façon on peut leur faire confiance (ou pas).
  * Les participants sont invités à devenir auteurs sur Wikipedia. Au
	menu du jour, se créer un compte, corriger quelques menues fautes
	repérées par avance, enrichir une page (par exemple la page
	française de Wikipedia sur
	[Armbouts Cappel](https://fr.wikipedia.org/wiki/Armbouts-Cappel) !),
  * enregistrement de photos de monuments ou de paysages dans
	[commons.wikimedia.org](https://commons.wikimedia.org).
	
## Samedi 22 décembre 9:30 à 11:30 ##
<img style="float: right; margin-left: 1ex; width: 240px;" alt="amap-numérique" src="images/amap_numerique.png">

  * Des outils Internet Éthiques : l'initialive
    [« Dégooglisons Internet »](https://degooglisons-internet.org/alternative)
    (2014-2017)
  * Débat : comment créer une « AMAP numérique », inspirée de la
	démarche « paysanne », Association pour le Maintien d'une
	Agriculture Paysanne.
  * Les techniques de communication « horizontale, pair à pair ».
  * Prise en main d'un ordinateur de moins de 40 euros,
    [Raspberry PI](http://www.raspberrypi.org/).
