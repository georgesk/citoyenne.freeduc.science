Title: Invitations, pour le 21 avril 2018
Date: Sat Mar 24 12:19:39 CET 2018
Modified: Sat Mar 24 12:19:39 CET 2018
Tags: programme
Category: invitations
Lang: fr

<img align="right" width="300" src="/images/img-21-04-2018.svg.png" alt="Encore douze sites qui m'espionnent !"/>
Par rapport au programme initial, il y a quelques modifications. À la fin de
la rencontre du 24 mars, il y a eu plus d'échanges « informels » que de
« recettes de cuisine » partagées. Ces échanges, très riches, étaient en très
grande partie une suite logique aux idées que le film **NOTHING TO HIDE**
de *Marc MEILLASSOUX* et *Mihaela GLADOVIC* avait amenées.


Le programme sera donc :

  * Présentation d'outils pour reprendre le contrôle de nos
données personnelles : moteur de recherche plus respectueux, plugins anti-espionnage, etc.
  * Recettes pour que chacune et chacun puisse mieux protéger sa vie
privée : on les mettra en pratique tout de suite.
  * Prise en main d'outils collaboratifs : les
    [PADs](https://framapad.org/)
  * Les participants démarrent un ou plusieurs PADs sur un ou
plusieurs sujet, par exemple : recettes « maison », préparation d'un
évènement.

Vous êtes invité.e.s à apporter un ordinateur portable ; la médiathèque
dispose de chaises, de tables en bon état, mais surtout d'un câblage qui
fournit toutes les prises de courant et de données que l'on puisse rêver
pour ce type de rencontre, et une connectivité correcte à Internet, soit
par connexion filaire, soit par WIFI.

Il y a aussi quelques ordinateurs fixes sur place, suffisamment récents
pour que leur BIOS autorise le démarrage à partir d'un disque USB : on pourra
aussi les utiliser.

Nous pourrons accéder à toutes les activités en utilisant n'importe quel
type de système pour les ordinateurs, Windows, Mac OS ; pour les
audacieuses et les audacieux, il sera possible de démarrer (booter) leur
ordinateur à l'aide d'une **CLÉ USB VIVE**, c'est à dire une clé USB ordinaire
sur le plan matériel, mais configurée de sorte à savoir démarrer (booter)
un ordinateur, détecter le matériel présent sur la carte mère, et le piloter,
puis démarrer un bureau graphique et des application. Le système utilisé
est **Debian GNU/Linux**, dans la mesure où l'animateur fait partie de 
l'organisation qui développe et maintient ce système-là, et qu'il en
possède un peu d'expertise, mais surtout une *confiance raisonnable dans le réseau des personnes* qui y contribuent.

Si vous le voulez, vous pourrez acheter la clé USB utilisée durant la formation,
contre dix euros. Il s'agira d'une clé USB de format 16 giga-octets,
communiquant à travers un port USB 3.0, ce qui assure une vélocité
confortable pour ce type d'usage.

Vous pouvez aussi, si vous préférez, et si vous faites partie des audacieux
bien sûr, apporter une clé USB de capacité différente (supérieure), et 
éventuellement plus réactive. Le type de matériel qui serait vraiment bien
adapté, ce sont les petites puces de mémoire flash, au format micro-SD, de
qualité "10" : on les connecte à l'aide d'un adaptateur USB, ou mieux encore,
elles se branchent à votre ordinateur portable dans le lecteur dédié.

<a href="/images/invitation-21-04-2018.pdf" title="Cliquer pour un document imprimable, au format PDF">
<img src="/images/invitation-21-04-2018.svg.png" alt="invitations" style="width: 600px;"/>
</a>
