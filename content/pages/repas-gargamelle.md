Title: Repas de Gargamelle
Date: Sun Apr 22 12:22:59 CEST 2018
Modified: Sun Apr 22 12:22:59 CEST 2018
Tags: données
Category: divers
Lang: fr

L’occasion & maniere comment Gargamelle enfanta fut telle. Et, si ne le croyez, le fondement vous escappe. Le fondement luy escappoit vne apresdinee, le iij. iour de feburier, par trop avoir mangé de gaudebillaux. Gaudebillaux : sont grasses tripes de coiraux. Coiraux : sont beufz engressez à la creche & prez guimaulx. Prez guimaulx : sont qui portent herbe deux fois l’an. D’iceulx gras beufz auoient faict tuer troys cens soixante sept mille & quatorze, pour estre à mardy gras sallez : affin qu’en la prime vere ilz eussent beuf de saison à tas, pour au commencement des repastz faire commemoration de saleures & mieulx entrer en vin.

Les tripes furent copieuses, comme entendez : & tant friandes estoient que chascun en leichoit ses doigtz. Mais la grande diablerie à quatre personnaiges estoit bien en ce que possible n’estoit longuement les reseruer, car elles feussent pourries. Ce que sembloit indecent. Dont fut conclud qu’ils les bauffreroient sans rien y perdre. A ce faire conuierent tous les citadins de Sainnais, de Suillé, de la Rocheclermaud, de Vaugaudray, sans laisser arriere le Coudray, Montpensier, le Gué de Vede et aultres voisins : tous bons beuueurs, bons compaignons, & beaulx ioueurs de quille là. Le bon homme Grandgousier y prenoit plaisir bien grand : & commendoit que tout allast par escuelles. Disoit toutesfoys à sa femme qu’elle en mangeast le moins, veu qu’elle aprochoit de son terme, et que ceste tripaille n’estoit viande moult louable. Celluy (disoit il) a grande enuie de mascher merde, qui d’icelle le sac mangeue. Non obstant ces remonstrances, elle en mangea seze muiz, deux bussars & six tupins. O belle matiere fecale, qui doiuoit boursouffler en elle !

Apres disner tous allerent (pelle melle) à la saulsaie : et là sus l’herbe drue dancerent au son des ioyeux flageolletz, & doulces cornemuses : tant baudement que c’estoit passetemps celeste les veoir ainsi soy rigouller.
