Title: Menu gastronomique
Date: Sun Apr 22 12:22:59 CEST 2018
Modified: Sun Apr 22 12:22:59 CEST 2018
Tags: données
Category: divers
Lang: fr

 Pour débuter le repas, quelques amuse-bouches accompagnés d'un kir pétillant

*****

**Le foie gras poêlé**

Au sirop d'érable et son toast brioché

*****

**Le veau**

Le filet cuit rosé, jus au thym, polenta aux fruits secs et tomates confites

OU

**Le bar**

Le filet grillé et son beurre Nantais, crémeux de céleri et asperges

*****

** La fourme de Montbrison **

Travail autour de la fourme en chaud froid

*****
**Entremet Fraise et Vanille**

 
*****
** Boissons :**

 *   Une bouteille pour 4 personnes de vin rouge ou blanc au choix du sommelier
 *   Une bouteille pour 3 personnes d'eaux minérales
 *   Café, thé ou infusion
