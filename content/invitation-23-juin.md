Title: Invitations, pour le 23 juin 2018
Date: Sat Jun  9 18:42:00 CEST 2018
Modified: Sat Jun  9 18:42:00 CEST 2018
Tags: programme
Category: invitations
Lang: fr

# Échanger sans être espionné #

<img align="right" width="300" src="/images/crypto.png" alt="échanges de messages chiffrés"/>

La rencontre commence par une mini-conférence : Alice envoie à Bob un message
chiffré. Comment faire ?

Eh bien, Alice et Bob se sont mis d'accord, ils ont installé quelques logiciels : GnuPG, Thunderbird, et son plugin Enigmail. Tous deux créent des clés cryptographiques, dont une sur deux est publique et peut être échangée au vu et au su de tous, sans compromettre en rien la sécurité des communications futures.

Notez bien que sur l'image ci-dessus, Alice dit très exactement :

    -----BEGIN PGP MESSAGE-----
    
    hQIMA2p80IRdkd1qARAAjdH628KnVRreloIsh7obhxaUxFX670foe/dL9Z/XgUDh
    hkUJcjWd2Wu3kr0c4SVpt8KU23ybZQZizbhKe7/e1/raP0RGYHPEc19XVhxVzg40
    YnAbAJvoA44DWStVR9oeaUTC5c9ANUS4qF1caTtAjvUNhB8f9MMUMYiZTUjGY8Ln
    7EudV1TphP8IIzn6oGfHKpnnCso3maB6GXpgDIj7vLN+6M6tTDvODE77+phVbHJI
    rK4UUtcaWno1O0sbZDST+frEggTpIGZaxybwbECTI42WlLKJ4MJ7iJ0Vr6xPnvbV
    CP9w6DErqe7gRuKEamQH3JIj0jknQ9vww+jMZ0+Xpd9UmxTrdO9MlIqxLiqB8alb
    xSgS0s263NFZcBh2GAOHTqMMqPgPQNZq8tU3mm51sEi6UR1hcAALEPXTnUoP+hy6
    rR64KZFhXwNK6BlDc9BMLdZLFDCJj2AgD5eUAqt627VdP/lCJ1YQKYEVUP3jSDZK
    FX+A5uMravx5VLCpwfKo8l6o+OH22vtog6e+Hxwmbo5cx6r3uL7AFUTHWoEKp5md
    vmKQ89iItWEDK+MXnVVXnMpEdIIx2iiMPRzg0gwJpKBZpyVf38T/MRaTW1C1wQLJ
    4zXXQYsL0hVgXAeBK05CPIDEZgHTwwc9iuDvVadOUGrLqy/neWviuZ09wuoY+JjS
    owG2neFsN+uJ6CGgn/7qff6T2Q3Os/5wHWtzsaMswqEZnYT11ulZITvoFFe0TvwL
    Wi6LH7vHlaCVCkKQIHDoen5nRytxauSGteIRbln8YoXB0T7pOr47E+csg/bqC/4s
    0D0JP/5q7gIRQF3rUfFIt+0VZA+K04HO1MNTtecqJMaLgekhbnqw2QiqJIXoOxmM
    /UO7zKe4Qh9tUm2brFNUiv50cuc=
    =j7jB
    -----END PGP MESSAGE-----
	
Bob, qui a la bonne clé, est le seul à pouvoir lire : « **Bob, rejoins-moi à la médiathèque d'Armbouts-Cappel samedi 23 juin ! il s'y passe des choses.** » ;
de plus Bob peut être certain que le message vient d'Alice et qu'il n'a
été altéré d'aucune façon, si Alice a pris le soin de le signer en le chiffrant.

# Scribus ... pour aller plus loin #

Le texte de la mini-conférence peut être imprimé sous forme d'un mini-livre,
à l'aide d'une imprimante simple. Un « modèle » de petit livre est accessible
[en cliquant ici](data/petitlivre.sla "Cliquez, puis de préférence « enregistrez sous ... »").

On peut changer les texte-modèle du document scribus, par exemple en prenant
le texte de la mini-conférence dans le fichier [gnupg.txt](data/gnupg.txt).
