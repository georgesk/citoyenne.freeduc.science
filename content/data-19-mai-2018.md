Title: Fichiers de données, pour le 19 mai 2018
Date: Sun Apr 22 12:22:59 CEST 2018
Modified: Sun Apr 22 12:22:59 CEST 2018
Tags: données
Category: divers
Lang: fr

Les données accessibles par cette page peuvent éventuellement servir
pour les activités de la rencontre du 19 mai 2018, où nous travaillerons des
textes et des images simples, à l'aide des logiciels libres Scribus,
Inkscape, Xia.

# Comment Gargamelle, estant grosse de Gargantua, mangea grand planté de tripes. #

 *   Titre : Gargantua
 *   Auteur(s) : Rabelais
 *   Éditeur : Alphonse Lemerre
 *   Publication : 1868
 *   Lieu d'édition : Paris
 *   Pages : 19-20
 *   Licence : domaine public
 
<a href="/pages/repas-de-gargamelle.html" title="Cliquer pour le texte complet">
  L’occasion & maniere comment Gargamelle enfanta fut telle. Et, si ne le croyez, le fondement vous escappe. ...
</a>

# Gravure : l'enfance de Pantagruel #

 *   Titre : Enfance de Pantagruel. Au berceau, le géant boit le lait de 4 600 vaches. 
 *   Auteur(s) : Gravure de Gustave Doré, chapitre IV de Pantagruel
 *   Date : téléversé le 2 mai 2013 à Mediawiki Commons
 *   Source: Bibliothèque nationale de France
 *   Éditeur : Garnier Frères
 *   Publication : 1873
 *   Lieu d'édition : Paris
 *   Licence : domaine public
 
<a href="/images/Pantagruel_s_childhood.jpg" title="Cliquez pour avoir l'image en haute résolution">
<img src="/images/Pantagruel_s_childhood-mini.jpg" alt="Pantagruel jeune (miniature)" width="300"/>
</a>

# Encre & gouache : l'enfance de Pantagruel #

 *   Titre : Enfance de Pantagruel.
 *   Auteur(s) : Gravure de Gustave Doré, dessin à l’encre brune, aquarelle, crayon et gouache sur papier 36 x 47,8 cm
 *   Date : téléversé le 23 avril 2011 à Mediawiki Commons
 *   Source: Musée d'Art Moderne et Contemporain, Strasbourg (achat en 1992)
 *   Publication : 1873
 *   Licence : domaine public

<a href="/images/Enfance_de_Pantagruel.jpg" title="Cliquez pour avoir l'image en haute résolution">
<img src="/images/Enfance_de_Pantagruel-mini.jpg" alt="Pantagruel jeune (miniature)" width="300"/>
</a>

# Un menu gastronomique #

 *   Titre : Menu Fête des mères O'Pavillon
 *   Auteur(s) : Restaurant La Charpinière
 *   Date : recopié le 22 avril 2018
 *   Source: http://www.lacharpiniere.com/fr/restaurant-gastronomique-menu-fete-des-meres-o-pavillon/famille-rest-fdm18pav.html
 *   Publication : 2018
 *   Licence : non précisée ; peut s'utiliser dans le cadre de l'usage de « courte citation »
 
 <a href="/pages/menu-gastronomique.html" title="Cliquer pour le texte complet">
Pour débuter le repas, quelques amuse-bouches accompagnés d'un kir pétillant ...
</a>

# Mosaïque en LEGO : Hip Hip Hourrah #

 *   Titre : Hip, Hip, Hurrah!
 *   Auteur(s) : peinture de Peder Severin Krøyer, recréée au festival danois de l'imagination, mosaïque en LEGO, St Katharine Docks, août 2012.
 *   Date : téléversé le 20 août 2012 à Mediawiki Commons
 *   Publication : 2012
 *   Licence :  CC-By-Sa : Creative Commons Attribution-Share Alike 3.0 Unported

<a href="/images/Cmglee_Lego_Hip_Hip_Hurrah.jpg" title="Cliquez pour avoir l'image en haute résolution">
<img src="/images/Cmglee_Lego_Hip_Hip_Hurrah-mini.jpg" alt="Pantagruel jeune (miniature)" width="300"/>
</a>
