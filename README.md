# Source du site web [citoyenne.freeduc.science](https://citoyenne.freeduc.science) #

Le site [citoyenne.freeduc.science](https://citoyenne.freeduc.science)
est formé de page HTML statiques, sa structure est recalculée grâce 
au logiciel [Pelican](http://pelican.readthedocs.org/), qui fonctionne
sous [Python](https://www.python.org/).

Les logiciels [Pelican](http://pelican.readthedocs.org/) (anagramme de
*calepin* producteur de sites web statiques) et
[Python](https://www.python.org/) sont tous deux des logiciels libres.

Le présent dépôt contient dans le sous-répertoire `content` le
texte des pages qui seront publiées sur le site 
[citoyenne.freeduc.science](https://citoyenne.freeduc.science).

Le thème utilisé pour les pages est dû à *Sam Hocevar*, qui a publié
le thème `dev-random2` pour Pelican, sous une licence
« DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE » en 2004 ; cette
licence est équivalente au Domaine Public. Ce thème est légèrement 
modifié par rapport à sa conception originale.
