#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'LLN'
AUTHORS = [u'LLN',]
BLOG_AUTHORS = [u'LLN',]
SITENAME = u'Les Libertés Numériques'
SITEURL = 'https://citoyenne.freeduc.science'
LANDING_PAGE_ABOUT="pages/page-daccueil.html"

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_RSS="feeds/rss.xml"


# Blogroll
LINKS = (
    ("Framasoft","https://framasoft.org/"),
    ("La Quadrature du Net","https://www.laquadrature.net/fr/"),
)

# Social widget
SOCIAL = (('Mastodon', 'https://mastodon.social/about'),
          ('Diaspora-Fr', 'https://diaspora-fr.org/'),)

DEFAULT_PAGINATION = 10

#THEME = '../pelican-themes/bluegrasshopper'
##THEME = '../pelican-themes/BT3-Flat'
##THEME = '../pelican-themes/chunk'
##THEME = '../pelican-themes/brownstone'
##THEME = '../pelican-themes/tuxlite_zf'
###THEME = '../pelican-themes/lightweight'
###THEME = '../pelican-themes/storm'
###THEME = '../pelican-themes/notebook'
###THEME = '../pelican-themes/blueidea'
###THEME = '../pelican-themes/lannisport'
###THEME = '../pelican-themes/pelican-cait'
###THEME = '../pelican-themes/nest'
###THEME = '../pelican-themes/pelican-twitchy'
###THEME = '../pelican-themes/zurb-F5-basic'
###THEME = '../pelican-themes/cebong'
####THEME = '../pelican-themes/dev-random'
####THEME = '../pelican-themes/voidy-bootstrap'
####THEME = '../pelican-themes/waterspill'
####THEME = '../pelican-themes/notmyidea-cms-fr'
####THEME = '../pelican-themes/dev-random2'
THEME = 'themes/my-dev-random2'

PAGE_PATHS = ['pages','images/favicon.png']
STATIC_PATHS = ['images','data']
EXTRA_PATH_METADATA = {
    'images/favicon.png': {'path': 'favicon.png'}
}

SLUGIFY_SOURCE = 'title'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
